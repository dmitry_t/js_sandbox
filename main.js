const dropZone = document.querySelector('#drop-zone');
const plus = document.querySelector('#plus');




const handleDrop = function (evt) {
    evt.stopPropagation();
    evt.preventDefault();
    let files = evt.dataTransfer.files;
    if (files.length == 1) {
        let file = files[0];
        let reader = new FileReader();
        reader.readAsText(file);
        reader.onload = function (event) {
            console.log(event.target.result);
        };
    }
}

const handleDragOver = function (evt) {
    dropZone.querySelector('h1').style.color = 'rgba(0,0,0,0)';
    dropZone.style.borderColor = 'rgb(248, 103, 103)';
    evt.stopPropagation();
    evt.preventDefault();
    evt.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
}
const handleDragLeave = function (evt) {
    dropZone.querySelector('h1').style.color = '#000';
    dropZone.style.color = '';
    dropZone.style.borderColor = '';
    evt.stopPropagation();
    evt.preventDefault();
    evt.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
}

if (window.File && window.FileList && window.FileReader) {
    dropZone.addEventListener('drop', handleDrop, false);
    dropZone.addEventListener('dragover', handleDragOver, false);
    dropZone.addEventListener('dragleave', handleDragLeave, false);
}


const is2dArray = arr => (Array.isArray(arr) && Array.isArray(arr[0]));





const arrayToText = function (arr, separator) {
    if (is2dArray(arr)) {
        return arr.reduce((accumulator, current) => {
            return accumulator + current.join(separator) + '\n';
        }, "");
    }
    console.log('not 2d array');
    return null;
}

const textToArray = function (content, separator) {
    let arr = [];
    for (let row of content.split('\n')) {
        row = row.split(separator);
        if (row.some(el => { return el; })) {
            arr.push(row);
        }
    }
    return arr;
}

const deleteBlankRows = function (arr) {
    if (is2dArray(arr)) {
        let resultArr = [];
        for (let row of arr) {
            if (row.some(el => {
                return el; // if not empty string or undefined
            })) {
                resultArr.push(row);
            }
        }
        return resultArr;
    }
    return null;
}

const deleteExcessCols = function (arr, cols) {
    let resultArr = [];
    for (let row of arr) {
        let tempRow = [];
        for (let index of cols) {
            if (row[index]) {
                tempRow.push(row[index]);
            }
        }
        resultArr.push(tempRow);
    }
    return resultArr;
}

const createTable = function (arr) {
    // table
    let table = document.createElement('table');
    table.className = 'table';
    //head
    let thead = document.createElement('thead');
    thead.className = 'thead-dark';
    let tr = document.createElement('tr');
    for (let col of arr[0]) {
        let column = document.createElement('th');
        column.textContent = col;
        tr.appendChild(column);
    }
    thead.appendChild(tr);
    // body
    let tbody = document.createElement('tbody');
    for (let row of arr.slice(1)) {
        let tr = document.createElement('tr');
        for (let cell of row) {
            let td = document.createElement('td');
            td.textContent = cell;
            tr.appendChild(td);
        }
        tbody.appendChild(tr);
    }
    table.appendChild(thead);
    table.appendChild(tbody);
    return table;
}


let content = `						
						
well	field	alt	md	x	y
12R	Upper-Salym	56	3200	15320	50675 dsdsds
13R	Upper-Salym	57	3150	15673	50960
14R	West-Salym	59	3230	15878	50470
1W	West-Salym	60,2	1800	15396	50788
2W	West-Salym	60,4	1780	15573	50643
well	field	alt	md	x	y
12R	Upper-Salym	56	3200	15320	50675 dsdsds
13R	Upper-Salym	57	3150	15673	50960
14R	West-Salym	59	3230	15878	50470
1W	West-Salym	60,2	1800	15396	50788
2W	West-Salym	60,4	1780	15573	50643
well	field	alt	md	x	y
12R	Upper-Salym	56	3200	15320	50675 dsdsds
13R	Upper-Salym	57	3150	15673	50960
14R	West-Salym	59	3230	15878	50470
1W	West-Salym	60,2	1800	15396	50788
2W	West-Salym	60,4	1780	15573	50643
`;





let arr = textToArray(content, '\t')
deleteBlankRows(arr);
arr = deleteExcessCols(arr, [1, 2, 7])
let table = createTable(arr);

// document.querySelector('#paste-btn').addEventListener('click', evt => {
//     document.querySelector('#table-area').appendChild(table);
//     $("#drop-zone").fadeOut(1000);
// })
// document.querySelector('#clear-btn').addEventListener('click', evt => {
//     document.querySelector('#table-area').innerHTML = '';
//     $("#drop-zone").fadeIn(1000);
// })



$(document).ready(function () {
    $('#paste-btn').click(function () {
        console.log('click');
        $("#drop-zone").slideToggle(1000);
    })
});